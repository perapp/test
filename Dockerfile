FROM python

RUN sed -i "s/^# alias /alias /" $HOME/.bashrc

ADD . /app/
WORKDIR /app
RUN pip install -r requirements.txt

RUN ln -s /app/bin/start /
RUN ln -s /app/bin/exec /
RUN ln -s /app/bin/build /
RUN ln -s /app/bin/test /
RUN chmod +x /{start,exec,build,test}

CMD python myapp/main.py
